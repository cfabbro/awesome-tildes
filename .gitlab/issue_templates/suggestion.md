# Suggestion Template

### Required

* Description:

### Optional

<!-- Please try to keep the suggestion itself and the reason for suggesting it separate. This makes it easier to review what the suggestion actually is. A good reason will make it more likely that the suggestion is approved. -->

* Reason for suggestion:
