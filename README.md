# Awesome Tildes

> A list of client-side extensions, CSS themes, and scripts developed and maintained by and for [tildes.net](https://tildes.net) users.

## Adding to This List

1. Fork the repository.
2. Add your entry with relevant information in the correct section.
3. Push the changes.
4. Submit a merge request.

OR 

1. Create a new issue with the entry template.
2. Fill in the information.
3. Submit the issue.

## Extensions

| Name | Author | Description |
| ---- | ------ | ----------- |
| [Extended](https://github.com/theCrius/tildes-extended) | [crius](https://tildes.net/user/crius) | Extension for Chrome and Firefox to enhance the UX/UI. |

## Scripts

## CSS Themes

| Name | Author | Description |
| ---- | ------ | ----------- |
| [Compact](https://gitlab.com/Bauke/styles/tree/master/css/tildes-compact) | [Bauke](https://tildes.net/user/Bauke) | Removes some elements and changes some sizes to make the layout a little more compact. |
| [Dracula](https://gitlab.com/Bauke/styles/tree/master/css/tildes-dracula) | [Bauke](https://tildes.net/user/Bauke) | Dracula theme. |
| [Monokai](https://gitlab.com/Bauke/styles/tree/master/css/tildes-monokai) | [Bauke](https://tildes.net/user/Bauke) | Monokai theme. |
| [Dark & Flat](https://gitlab.com/jfecher/Tildes-DarkAndFlat) | [rndmprsn](https://tildes.net/user/rndmprsn) | A dark and flat theme + alt. version combined with [pfg](https://tildes.net/user/pfg)'s [Collapse Comment Bar](https://gist.github.com/pfgithub/1da7d2024a9748085fccfceaf2ce126d) |